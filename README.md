# "Text Magic" Project Using Python #

This project is based on Python 2.7, which by default installed on Mac/Ubuntu OS

### What "Text Magic" Do ###

* Web crawling different websites using Python package (urllib2 or selenium)
* Extract the information to plain text, or CSV file
* Use Natural Language Processing package (nltk) to pre-process text, thne get text statistic
* Use Machine Learning package (numpy, scipy, scikit-learn) to apply regression, classification and clustering

### How do I get set up? ###

* Make sure Python2.7 is installed on your computer
* Make sure the latest Firefox browser is installed
* Install pip and upgrade it to current version by: 
    * Install pip for Mac: ```sudo easy_install pip```
    * Install pip for Ubuntu: ```sudo apt-get install python-pip```
    * Upgrde pip: ```sudo -H pip install --upgrade pip```
* Install selenium: ```sudo -H pip install selenium ```
* Install BeautifulSoup4: ```sudo -H pip install beautifulsoup4```
* Install nltk: ```sudo -H pip install nltk```
    * You also need to download the resources in nltk 
    * First open terminal, and type ```python```
    * Inside the python prompts, type ```import nltk```, then press "Enter" to go to next line
    * Type ```nltk.download('all')``` and press "Enter", it will download around 700MB of resources 

* For some scripts, you may also need to install scipy, scikit-learn. Please refer to http://scikit-learn.org/stable/ to check installation instruction for your OS, here's the example for Ubuntu:
    * Install SciPy Stack (which is pre-requisite for scikit-learn): 
        ```sudo apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose```
    * Install scikit-learn using pip
        ```sudo -H pip install -U scikit-learn```  NOTICE: This may take as long as 5 minutes

### When in doubt, try Python! ###
