import os
import string
import re
import urllib2
import unicodedata
import nltk
from bs4 import BeautifulSoup

url = 'http://www.ascentis.com/'
# Get the raw html from a url
response = urllib2.urlopen(url)
html = response.read()

# Convert the raw html (plain text) to Beautiful Soup format
soup = BeautifulSoup(html, "html.parser")

# Get rid of script, meta, style tag, and get texts from other fields
[s.extract() for s in soup('script')]
[s.extract() for s in soup('meta')]
[s.extract() for s in soup('style')]

all_text = soup.get_text()

# Process the text by
#   Remove extra spaces
#   Remove empty lines
#   Convert/remove unicode
#   Convert to lower case, remove punctuation
#   Remove words length < 3
all_text = os.linesep.join([s.strip() for s in all_text.splitlines() if s])
all_text = all_text.replace("\n", " ")
all_text = unicodedata.normalize('NFKD', all_text).encode('ascii','ignore')
all_text = all_text.strip().lower().translate(None, string.punctuation)
all_text = re.sub(r'\b\w{1,3}\b', '', all_text)
all_text = re.sub(r'[-.?!,":;()|0-9]', '', all_text)

print all_text + "\n"

# Tokenize the text, and get rid of stop words
tokens = nltk.word_tokenize(all_text)
filtered_words = [w for w in tokens if not w in nltk.corpus.stopwords.words('english')]

print filtered_words

#Get the 10 most frequent words
freqent_words = nltk.FreqDist(filtered_words)

print "\n \n \n \n \n \n \n \n The key words for website '" + url + "'' is: \n"
print freqent_words.most_common(10)
