#!/usr/bin/env python
import string
import random
from time import sleep
from selenium import webdriver

def main():

    # Define n as # of 'list of pages' and i as the item # of the certain list

    n = 243
    i = 20

    while n <= 243:
        while i <= 20:
            # sleep(random.randrange(2,12,1)/8.7)
            global driver
            driver = webdriver.Firefox()
            # driver = webdriver.Chrome('/Users/gxc/Desktop/textmagic/web_crawling/chromedriver')
            driver.get(base_url) # load page
            sleep(1)
            try:
                print 'n is: ' + str(n) + '; i is: ' + str(i)
                if n >=2:
                    link = driver.find_element_by_link_text(str(n))
                    # get individual 1,2,3,4 page lists
                    # sleep(random.randrange(2,15,1)/8.2)
                    driver.get(link.get_attribute('href')) 
                item_number = str(i).zfill(2)
                item_id = item_id_prefix + item_number + item_id_postfix
                # based on the item html id, extract information from the corresponding page
                extract_item_text(item_id)

            except:
                error = 'something wrong at ' + 'n is: ' + str(n) + '; i is: ' + str(i)
                print error
                f2 = open('error.txt', 'a+')
                f2.write(error+'\n')
                f2.close()
                pass

            driver.quit()
            i += 1


        n += 1
        i = 1
        print 'finish list' + str(n)


def extract_item_text(item_id):

    print 'extracting the ext for id:' + item_id
    item = driver.find_element_by_id(item_id)
    item_link = item.find_element_by_tag_name('a')
    item_href = item_link.get_attribute('href')

    print item_href
    driver.get(item_href)
    basic_info = driver.find_element_by_id('ctl00_ContentPlaceHolder1_AuctionInformationBox1_tblBidInformation')
    specs_info = driver.find_element_by_id('tblSpecs')

    process_text(basic_info.text, specs_info.text)

def process_text(basic, specs):

    all_info = basic + '\n' + specs + '\n'
    all_info = all_info.encode('ascii','ignore')
    all_info = all_info.replace(':' , '')
    all_info = all_info.replace(',' , '')
    tokenized_info = all_info.split('\n')
    print tokenized_info

    for key in fields_value:
        for tokenized_item in tokenized_info:
            if key in tokenized_item:
                if key is "Bids":
                    fields_value[key] = tokenized_item.split(key)[0].strip()
                else:
                    fields_value[key] = tokenized_item.split(key)[1].strip()

    new_csv_line = ''
    for field in fields:
        new_csv_line = new_csv_line + fields_value[field] + ','
    new_csv_line = new_csv_line[0:-1]
    # sleep(random.randrange(5,15,1)/10.7)
    print new_csv_line
    f = open('fields.csv', 'a+')
    f.write(new_csv_line + '\n')
    f.close()

if __name__ == '__main__':
    
    driver = webdriver.Firefox()
    driver.close()

    base_url = 'http://www.auctiontime.com/list/list.aspx?oalresults=1&EventBD=11%2f25%2f2013&EventED=11%2f25%2f2015&etid=6&LP=TRK&bcatid=27'
    item_id_prefix = 'ctl00_ContentPlaceHolder1_ctl31_rpResults_ctl'
    item_id_postfix = '_tdOALResultsGeneralInfo'

    fields = ['Final Bid', 'Opened At', 'Bids', 'Sale Ended', 'Year', 'Manufacturer', 'Model', 'Location', 'Condition','Sleeper Size','Sleeper', 'Engine Manufacturer', 'Engine Type', 'Horsepower', 'Mileage', 'Fuel Type', 'Transmission', 'Overdrive' ,'Engine Brake', 'Suspension', 'Ratio', 'Tires', 'Wheels', 'Wheelbase', 'Number of Rear Axles', 'Front Axle Weight', 'Rear Axle Weight', 'VIN', 'Gross Vehicle Weight', 'Drive Side']

    fields_header = ''
    for field in fields:
        fields_header = fields_header + field + ','
    fields_header = fields_header[0:-1]

    f0 = open('error.txt','w+')
    f0.write('error log start from line:'+'\n')
    f0.close()

    # f = open('fields.csv', 'a+')
    # f.write(fields_header+'\n')
    # f.close()

    fields_value = {  
                    'Final Bid': 'NA',
                    'Opened At': 'NA',
                    'Bids': 'NA',
                    'Sale Ended': 'NA',
                    'Year': 'NA',
                    'Manufacturer': 'NA',
                    'Model': 'NA',
                    'Location': 'NA',
                    'Condition': 'NA',
                    'Sleeper Size': 'NA',
                    'Sleeper': 'NA',
                    'Engine Manufacturer': 'NA',
                    'Engine Type': 'NA',
                    'Horsepower': 'NA',
                    'Mileage': 'NA',
                    'Fuel Type': 'NA',
                    'Transmission': 'NA',
                    'Overdrive': 'NA',
                    'Engine Brake': 'NA',
                    'Suspension': 'NA',
                    'Ratio': 'NA',
                    'Tires': 'NA',
                    'Wheels': 'NA',
                    'Wheelbase': 'NA',
                    'Number of Rear Axles': 'NA',
                    'Front Axle Weight': 'NA',
                    'Rear Axle Weight': 'NA',
                    'VIN': 'NA',
                    'Gross Vehicle Weight': 'NA',
                    'Drive Side': 'NA'
                }

    main()


